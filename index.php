<?php

	$api_key = 'key0C9imU34S0P8Ds';
	$url = 'https://api.airtable.com/v0/apps4HReU8ymdp4iE/Table%201?api_key=key0C9imU34S0P8Ds&sort%5B0%5D%5Bfield%5D=Fecha&sort%5B0%5D%5Bdirection%5D=asc';
	
	$headers = array(
	    'Authorization: Bearer ' . $api_key
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_URL, $url);
	$entries = curl_exec($ch);
	curl_close($ch);
	$airtable_response = json_decode($entries, TRUE);
	$dataAll= array();

	foreach ($airtable_response['records'] as $fields){
		  array_push($dataAll, $fields['fields']);
	}
	
?>
<html>
  <head>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   
	  	<link rel="stylesheet" href="./style/style.css">
		  <link rel="stylesheet" href="./style/side-menu.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
		<script src="https://cdn.jsdelivr.net/npm/chart.js"></script> <!-- https://www.chartjs.org/ -->
   
  </head>
  <body>
	
	<div class="sidebar">
		<a class="button all " id="btnAll">Ambos</a>
		<a class="button red" id="btnPerson1" >Ye</a>
		<a class="button blue" id="btnPerson2">Alex</a>
	</div>

  	<div class="container">
	  	<div id="chart-container" >
	        <canvas class="graphDiv" id="commonGraph"></canvas>
	        <canvas class="graphDiv" id="person1graph"></canvas>
	        <canvas class="graphDiv" id="person2graph"></canvas>
	    </div>

		
    </div>

	<div class="footer">
		<p>Visualización en tiempo real de estadísticas corporales. </p>
		<p>PHP | Airtable/Api | HTML | CSS   </p>
		<p >Designed by -- <a href="https://yemafe.es/">Yemafe</a></p>
	</div>

		 <script type="text/javascript">
    			var dataAll = <?php echo json_encode($dataAll); ?>;
									
					var arrayCommon1 = [];
					var arrayCommon2 = [];
					var arrayLabel = [];
					//var arrayPerson1 = [];
					var arrayPerson1weight  = [];
					var arrayPerson1muscle  = [];
					var arrayPerson1fat  = [];
					var arrayPerson1IMC  = [];
					var arrayPerson2weight  = [];
					var arrayPerson2muscle  = [];
					var arrayPerson2fat  = [];
					var arrayPerson2IMC  = [];
					var arrayLabelPerson1 = [];
					var arrayLabelPerson2 = [];
					var arrayRespuesta  = [];

					for(var i in dataAll){
					    arrayRespuesta.push([i, dataAll [i]]);
					}
					
					for(var j=0; j<arrayRespuesta.length; j++){
						for(var i=0; i<arrayRespuesta[j].length; i++){
						
						    fieldData = arrayRespuesta[j][1];
							// ** YE -- mount array for each bar in the graphic then paint it in graph type bar 
							//console.log(fieldData); //

						    if(fieldData['Name'].includes('Ye')){
						    	if(i % 2 !=0){
									arrayLabelPerson1.push(fieldData['Fecha']);
							        arrayPerson1weight.push(fieldData['Peso']);
									arrayPerson1muscle.push(fieldData['Masa muscular'] * 100);
									arrayPerson1fat.push(fieldData['Grasa corporal']* 100 );
									arrayPerson1IMC.push(fieldData['IMC']);
							    }
						    }else if (fieldData['Name'].includes('Ale')){
						    	if(i % 2 !=0){
							        arrayLabelPerson2.push(fieldData['Fecha']);
							        arrayPerson2weight.push(fieldData['Peso']);
									arrayPerson2muscle.push(fieldData['Masa muscular'] * 100);
									arrayPerson2fat.push(fieldData['Grasa corporal']* 100 );
									arrayPerson2IMC.push(fieldData['IMC']);
							    }
						    }
							
						}
					}
				console.log(arrayPerson2muscle );


					for(var i in arrayPerson1weight ){
						for(var j in arrayPerson1weight ){
						   if((arrayPerson1weight [i]!== undefined) && (arrayPerson2weight [j]!== undefined)){
									if(arrayLabelPerson1[i]==arrayLabelPerson2[j]){
							      arrayLabel.push(arrayLabelPerson1[i]);
							      arrayCommon1.push(arrayPerson1weight [i]);
							      arrayCommon2.push(arrayPerson2weight [j]);
							    }
						  	}
						}
					}
				
			//*************  Common graphDiv 
				var chartdata = {
              labels: arrayLabel,
              datasets: [
                  {
                      label: 'Ye',
                      backgroundColor: '#ff5050',
                      borderColor: '#ff3333',
                      hoverBackgroundColor: '#CCCCCC',
                      hoverBorderColor: '#666666',
                      data: arrayCommon1
                  },
                  {
					label: 'Alex',
					backgroundColor: '#49e2ff',
                    borderColor: '#46d5f1',
                    hoverBackgroundColor: '#CCCCCC',
                    hoverBorderColor: '#666666',
                    data: arrayCommon2
					}
              ]
          };
          var graphTarget = $("#commonGraph");
          //config
          var barGraph = new Chart(graphTarget, {
              type: 'line',
              data: chartdata,
              options: {
						responsive: true,
						plugins: {
							legend: {
								position: 'top',
							},
							title: {
								display: true,
								text: 'Seguimiento peso y parámetros corporales.'
								}
							}
						}
          });

   
			//*************   person1graph
			/*
				var chartdata = {
              labels: arrayLabelPerson1,
              datasets: [
                  {
                      label: 'Ye',
                      backgroundColor: '#ff5050',
                      borderColor: '#ff3333',
                      hoverBackgroundColor: '#CCCCCC',
                      hoverBorderColor: '#666666',
                      data: arrayPerson1
                  }
              ]
          };
          var graphTarget = $("#person1graph");
          //config
          var barGraph = new Chart(graphTarget, {
              type: 'line',
              data: chartdata,
              options: {
						    responsive: true,
						    plugins: {
						      legend: {
						        position: 'top',
						      },
						      title: {
						        display: true,
						        text: 'Seguimiento peso y parámetros corporales.'
						      }
						    }
						  }
          });
		  */



		  var chartdata = {
              labels: arrayLabelPerson1,
              datasets: [
				  //line - weight
				{
					label: 'Kg',
					data: arrayPerson1weight,
					borderColor: 'red',
					backgroundColor: 'black',
					type: 'line',
					order: 0
				},
				{
					label: 'Grasa',
					data: arrayPerson1fat,
					borderColor: 'orange',
					backgroundColor: 'yellow',
					order: 1
				},
				{
					label: 'Músculo',
					data: arrayPerson1muscle,
					borderColor: 'red',
					backgroundColor: 'pink',
					order: 2
				},
				{
					label: 'IMC',
					data: arrayPerson1IMC,
					borderColor: 'green',
					backgroundColor: 'lime',
					order: 2
				},
              ]
          };
          var graphTarget = $("#person1graph");
          //config
          var barGraph = new Chart(graphTarget, {
              type: 'bar',
              data: chartdata,
              options: {
						    responsive: true,
						    plugins: {
						      legend: {
						        position: 'top',
						      },
						      title: {
						        display: true,
						        text: 'Parámetros corporales, ACTIVA o DESACTIVA cada uno!!'
						      }
						    }
						  }
          });









		//*************   person2graph
		  // type bar
		  //console.log(arrayPerson2fat );
		  var chartdata = {
              labels: arrayLabelPerson2,
              datasets: [
				  //line - weight
				{
					label: 'Kg',
					data: arrayPerson2weight,
					borderColor: 'blue',
					backgroundColor: 'black',
					type: 'line',
					order: 0
				},
				{
					label: 'Grasa',
					data: arrayPerson2fat,
					borderColor: 'orange',
					backgroundColor: 'yellow',
					order: 1
				},
				{
					label: 'Músculo',
					data: arrayPerson2muscle,
					borderColor: 'red',
					backgroundColor: 'pink',
					order: 2
				},
				{
					label: 'IMC',
					data: arrayPerson2IMC,
					borderColor: 'green',
					backgroundColor: 'lime',
					order: 2
				},
              ]
          };
          var graphTarget = $("#person2graph");
          //config
          var barGraph = new Chart(graphTarget, {
              type: 'bar',
              data: chartdata,
              options: {
						    responsive: true,
						    plugins: {
						      legend: {
						        position: 'top',
						      },
						      title: {
						        display: true,
						        text: 'Parámetros corporales, ACTIVA o DESACTIVA cada uno!!'
						      }
						    }
						  }
          });

        	$( document ).ready(function() {

        		$("#person1graph").hide();
        		$("#person2graph").hide();

	        	$( "#btnAll" ).click(function() {
					$('.graphDiv').hide();
					$("#commonGraph").show();
				});
	        	
	        	$( "#btnPerson1" ).click(function() {
						$('.graphDiv').hide();
						$("#person1graph").show();
					});

					$( "#btnPerson2" ).click(function() {
						$('.graphDiv').hide();
						$("#person2graph").show();
					});

				});
        </script>
  </body>
</html>
